# PyEle

Before use install dependencies:
```{r, engine='bash', count_lines}
python3 -m pip install numpy
```

## mseScope

Example usage
```{r, engine='bash', count_lines}
python3 mseScope.py -m 3 example_input/mseScope.txt example_output/mseScope.txt
```

Help:
```{r, engine='bash', count_lines}
usage: mseScope.py [-h] [-c] [-m MULTIPLY] input_file [output_file]

Convert multisim elvis scope to TXT - do not use at 00:00

positional arguments:
  input_file            Multisim elvis scope output file
  output_file           Output file - default stdout

optional arguments:
  -h, --help            show this help message and exit
  -c, --commas          Use commas instead of dots - supported only with real
                        file
  -m MULTIPLY, --multiply MULTIPLY
                        Repeat data m times
```