import argparse

import numpy as np
import re
import os
import sys

class MseScope(object):

    def __init__(self):
        self.data = np.empty((0, 3))

    def load(self, input_file: str) -> None:
        """
        :param input_file: input file
        """

        decimal_len = None  # used during detection of orginal time precision
        self.data = np.empty((0, 3))  # reset data

        with open(input_file) as inputHandler:
            # skip all lines before data
            for line in inputHandler:
                if line.startswith("time[0]"):
                    break

            for line in inputHandler:
                # convert input line to list
                line = line.strip()
                line = line.replace(",", ".")
                line = line.replace("\t", " ")
                line = re.sub(r" +", " ", line)  # remove duplicated spaces
                line = line.split(" ")

                # remove useless columns
                del(line[0])
                del(line[2])
                del(line[2])

                # decode time - date is omitted - !!! DO NOT USE AT 00:00 !!!
                if decimal_len is None:
                    decimal_len = len(line[0][9:])
                hour = float(line[0][0:2])
                minute = float(line[0][3:5])
                second = float(line[0][6:8])
                decimal = float("0."+line[0][9:])
                time = hour*3600 + minute*60 + second + decimal

                line[0] = time

                self.data = np.append(self.data, [line], axis=0)

            self.data = self.data.astype(np.float)  # convert all to float

            self.data -= [self.data[0][0], 0, 0]  # offset time to start at 0

            # add suffix to duplicated times
            # detect
            time0_count = 0
            look_time = None
            duplicates_count = 0
            for line in self.data:
                if line[0] == 0:
                    time0_count += 1
                    continue
                if look_time is None:
                    look_time = line[0]
                if look_time != line[0]:
                    break
                duplicates_count += 1

            # add time suffix
            if duplicates_count > 1:
                dt = 10**-decimal_len / duplicates_count

                n = duplicates_count - time0_count
                for line in self.data:
                    line[0] += n * dt
                    n += 1
                    if n == duplicates_count:
                        n = 0

            self.data -= [self.data[0][0], 0, 0]  # offset time to start at 0

    def multiply(self, n: int) -> None:
        """
        Repeat data n times
        """
        orginal_data = np.copy(self.data)
        for _ in range(0, n-1):
            self.data = np.append(self.data, orginal_data+[max(self.get_t()), 0, 0], axis=0)

    def get_t(self):
        """
        :return: time array
        """
        return self.data[:, 0]

    def get_a(self):
        """
        :return: chanel A array
        """
        return self.data[:, 1]

    def get_b(self):
        """
        :return: chanel B array
        """
        return self.data[:, 2]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert multisim elvis scope to TXT - do not use at 00:00')
    parser.add_argument('input_file', action="store", help="Multisim elvis scope output file")
    parser.add_argument("output_file", action="store", nargs="?", default=None, help="Output file - default stdout")
    parser.add_argument("-c", "--commas", action="store_true", help="Use commas instead of dots - supported "
                                                                    "only with real file")
    parser.add_argument("-m", "--multiply", type=int, action="store", default=1, help="Repeat data m times")

    args = parser.parse_args()

    scope = MseScope()
    scope.load(args.input_file)

    if args.multiply > 1:
        scope.multiply(args.multiply)

    output_file = sys.stdout if args.output_file is None else args.output_file

    np.savetxt(output_file, scope.data, newline=os.linesep, fmt="%+.12f")

    if args.output_file is not None and args.commas:
        file = open(args.output_file, "r")
        content = file.read()
        file.close()

        content = content.replace(".", ",")

        file = open(args.output_file, "w")
        file.write(content)
        file.close()
